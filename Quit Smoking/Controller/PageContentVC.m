//
//  PageContentVC.m
//  Quit Smoking
//
//  Created by Quit Smoking on 24/04/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import "PageContentVC.h"

@implementation PageContentVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view = self.customView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
