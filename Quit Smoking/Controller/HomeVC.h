//
//  HomeVC.h
//  Quit Smoking
//
//  Created by Quit Smoking on 26/04/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC : UIViewController{
	IBOutlet UILabel *lblQuitDate;
    IBOutlet UILabel *lblTotalDays;
    IBOutlet UILabel *lblTotalCigarette;
    IBOutlet UILabel *lblTotalMoney;
    IBOutlet UITextView *txtTips;
    NSArray *arrMotivationalTips;
    NSInteger indexOfTips;
}
@end
