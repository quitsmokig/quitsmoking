//
//  ViewController.h
//  Quit Smoking
//
//  Created by Quit Smoking on 23/04/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentVC.h"

@interface ViewController : UIViewController <UIPageViewControllerDataSource, UITextFieldDelegate, UIActionSheetDelegate>{
    IBOutlet UIView *view1;
    IBOutlet UIView *view2;
    IBOutlet UIView *view3;
    IBOutlet UIView *view4;
    
    IBOutlet UITextField *txtNoOfCigDaily;
    IBOutlet UITextField *txtNoOfCigInBox;
    IBOutlet UITextField *txtPriceOfCigBox;
    IBOutlet UITextField *txtQuitDate;
    IBOutlet UITextField *txtQuitTime;
}

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageViews;

-(IBAction)selectDate:(id)sender;
-(IBAction)selectTime:(id)sender;
-(IBAction)goOn:(id)sender;
@end

