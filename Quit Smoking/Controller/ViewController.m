//
//  ViewController.m
//  Quit Smoking
//
//  Created by Quit Smoking on 23/04/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Common.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.navigationController setNavigationBarHidden:YES];
    // Change navigation title color white
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Create the data model
    _pageViews = @[view1, view2, view3, view4];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    //[self.navigationController setNavigationBarHidden:YES];
    
    AppDelegate* sharedDelegate = [AppDelegate appDelegate];
    
    NSManagedObjectContext *context = [sharedDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Setup" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    [fetchRequest setResultType:NSDictionaryResultType];
    //[fetchRequest setReturnsDistinctResults:YES];
    [fetchRequest setPropertiesToFetch:@[@"noOfCigDaily"]];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        // Handle the error.
        NSLog(@"Getting data failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
    }
    else{
        //NSLog(@"%@", fetchedObjects);
        // if user data not saved then save else goto home screen
        if (fetchedObjects.count <= 0) {
            //
            PageContentVC *startingViewController = [self viewControllerAtIndex:0];
            NSArray *viewControllers = @[startingViewController];
            [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
        }
        else{
            // goto home screen
            [self performSegueWithIdentifier:@"home" sender:self];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (PageContentVC *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageViews count] == 0) || (index >= [self.pageViews count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentVC *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentVC"];
    pageContentViewController.customView = self.pageViews[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentVC*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentVC*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageViews count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageViews count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == txtNoOfCigDaily){
        [txtNoOfCigInBox becomeFirstResponder];
    }
    else if (textField == txtNoOfCigInBox){
        [txtPriceOfCigBox becomeFirstResponder];
    }
    else if (textField == txtPriceOfCigBox){
        [txtPriceOfCigBox resignFirstResponder];
    }
    else if (textField == txtQuitDate){
        [txtQuitTime becomeFirstResponder];
    }
    else if (textField == txtQuitTime){
        [txtQuitTime resignFirstResponder];
    }
    return YES;
}

-(void)selectDate:(id)sender{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SELECT A DATE\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(-8, 44, 320, 200)];
    [picker setDatePickerMode:UIDatePickerModeDate];
    [alertController.view addSubview:picker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //NSLog(@"OK");
            //NSLog(@"%@",picker.date);
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"yyyy-MM-dd"];
            NSString *nsstr = [format stringFromDate:picker.date];
            
            [txtQuitDate setText:nsstr];
        }];
        action;
    })];
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
}

-(void)selectTime:(id)sender{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SELECT A TIME\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(-8, 44, 320, 200)];
    [picker setDatePickerMode:UIDatePickerModeTime];
    [alertController.view addSubview:picker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //NSLog(@"OK");
            //NSLog(@"%@",picker.date);
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"hh:mm a"];
            NSString *nsstr = [format stringFromDate:picker.date];
            
            [txtQuitTime setText:nsstr];
        }];
        action;
    })];
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
}

-(IBAction)goOn:(id)sender{
    NSString *message = @"";
    if (![txtNoOfCigDaily.text isEqualToString:@""]){
        message = [message stringByAppendingString:@"\nEnter number of cigarettes daily."];
    }
    if (![txtNoOfCigInBox.text isEqualToString:@""]) {
        message = [message stringByAppendingString:@"\nEnter number of cigarettes in box."];
    }
    if (![txtPriceOfCigBox.text isEqualToString:@""]) {
        message = [message stringByAppendingString:@"\nEnter price of cigarette box."];
    }
    if (![txtQuitDate.text isEqualToString:@""]) {
        message = [message stringByAppendingString:@"\nSelect cigarette quit date."];
    }
    if (![txtQuitTime.text isEqualToString:@""]) {
        message = [message stringByAppendingString:@"\nSelect cigarette quit time."];
    }
    
    if (![message isEqualToString:@""]){
        AppDelegate* sharedDelegate = [AppDelegate appDelegate];
        
        NSManagedObjectContext *context = [sharedDelegate managedObjectContext];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Setup" inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        [fetchRequest setResultType:NSDictionaryResultType];
        //[fetchRequest setReturnsDistinctResults:YES];
        [fetchRequest setPropertiesToFetch:@[@"noOfCigDaily"]];
        
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        if (fetchedObjects == nil) {
            // Handle the error.
            NSLog(@"Getting data failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
        }
        else{
            //NSLog(@"%@", fetchedObjects);
            // if user data not saved then save else goto home screen
            if (fetchedObjects.count <= 0) {
                NSManagedObject *setup = [NSEntityDescription insertNewObjectForEntityForName:@"Setup" inManagedObjectContext:context];
                [setup setValue:txtNoOfCigDaily.text forKey:@"noOfCigDaily"];
                [setup setValue:txtNoOfCigInBox.text forKey:@"noOfCigInBox"];
                [setup setValue:txtPriceOfCigBox.text forKey:@"priceOfCigBox"];
                [setup setValue:txtQuitDate.text forKey:@"quitDate"];
                [setup setValue:txtQuitTime.text forKey:@"quitTime"];
                
                // Save the context
                NSError *error = nil;
                if (![context save:&error]) {
                    NSLog(@"Saving Failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
                }
                else{
                    NSLog(@"Setup data saved!!");
                    
                    // schedule notification to show every day
                    // Get the current date and add 10 seconds extra to show 1st notification
                    NSDate *notificationDateTime = [NSDate dateWithTimeIntervalSinceNow:10];
                    
                    // create alert message
                    NSString *alertMessage = [NSString stringWithFormat:@"You quit smoke on %@. You did not smoke any cigarettes these days and saved money. Keep it up!! You are doing great!!", txtQuitDate.text];
                    
                    // Schedule the notification
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = notificationDateTime;
                    localNotification.alertBody = alertMessage;
                    localNotification.alertAction = @"OK";
                    localNotification.repeatInterval = NSCalendarUnitDay;
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                    
                    // goto home screen
                    [self performSegueWithIdentifier:@"home" sender:self];
                }
            }
            else{
                // goto home screen
                [self performSegueWithIdentifier:@"home" sender:self];
            }
        }
    }
    else{
        [Common showAlertWithTitle:@"Alert" andMessage:message andController:self];
    }
}

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([[segue identifier] isEqualToString:@"home"])
//    {
//        HomeVC *home = (HomeVC*)[segue destinationViewController];
//    }
//}
@end
