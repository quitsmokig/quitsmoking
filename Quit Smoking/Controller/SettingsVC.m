//
//  SettingsVC.m
//  Quit Smoking
//
//  Created by Quit Smoking on 01/05/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import "SettingsVC.h"
#import "AppDelegate.h"
#import "Common.h"

@implementation SettingsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Change navigation title color white
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    // show existing settings
    [self getSettings];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getSettings{
    AppDelegate* sharedDelegate = [AppDelegate appDelegate];
    
    NSManagedObjectContext *context = [sharedDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Setup" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    [fetchRequest setResultType:NSDictionaryResultType];
    //[fetchRequest setReturnsDistinctResults:YES];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        // Handle the error.
        NSLog(@"Getting data failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
    }
    else{
        NSLog(@"%@", fetchedObjects);
        [txtNoOfCigDaily setText:[[fetchedObjects objectAtIndex:0] valueForKey:@"noOfCigDaily"]];
        [txtNoOfCigInBox setText:[[fetchedObjects objectAtIndex:0] valueForKey:@"noOfCigInBox"]];
        [txtPriceOfCigBox setText:[[fetchedObjects objectAtIndex:0] valueForKey:@"priceOfCigBox"]];
        [txtQuitDate setText:[[fetchedObjects objectAtIndex:0] valueForKey:@"quitDate"]];
        [txtQuitTime setText:[[fetchedObjects objectAtIndex:0] valueForKey:@"quitTime"]];
    }
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == txtNoOfCigDaily){
        [txtNoOfCigInBox becomeFirstResponder];
    }
    else if (textField == txtNoOfCigInBox){
        [txtPriceOfCigBox becomeFirstResponder];
    }
    else if (textField == txtPriceOfCigBox){
        [txtPriceOfCigBox resignFirstResponder];
    }
    else if (textField == txtQuitDate){
        [txtQuitTime becomeFirstResponder];
    }
    else if (textField == txtQuitTime){
        [txtQuitTime resignFirstResponder];
    }
    return YES;
}

-(void)selectDate:(id)sender{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SELECT A DATE\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(-8, 44, 320, 200)];
    [picker setDatePickerMode:UIDatePickerModeDate];
    [alertController.view addSubview:picker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //NSLog(@"OK");
            //NSLog(@"%@",picker.date);
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"yyyy-MM-dd"];
            NSString *nsstr = [format stringFromDate:picker.date];
            
            [txtQuitDate setText:nsstr];
        }];
        action;
    })];
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
}

-(void)selectTime:(id)sender{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"SELECT A TIME\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(-8, 44, 320, 200)];
    [picker setDatePickerMode:UIDatePickerModeTime];
    [alertController.view addSubview:picker];
    [alertController addAction:({
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //NSLog(@"OK");
            //NSLog(@"%@",picker.date);
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"hh:mm a"];
            NSString *nsstr = [format stringFromDate:picker.date];
            
            [txtQuitTime setText:nsstr];
        }];
        action;
    })];
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = sender;
    popoverController.sourceRect = [sender bounds];
    [self presentViewController:alertController  animated:YES completion:nil];
}

-(IBAction)updateSetup:(id)sender{
    NSString *message = @"";
    if ([txtNoOfCigDaily.text isEqualToString:@""]){
        message = [message stringByAppendingString:@"\nEnter number of cigarettes daily."];
    }
    if ([txtNoOfCigInBox.text isEqualToString:@""]) {
        message = [message stringByAppendingString:@"\nEnter number of cigarettes in box."];
    }
    if ([txtPriceOfCigBox.text isEqualToString:@""]) {
        message = [message stringByAppendingString:@"\nEnter price of cigarette box."];
    }
    if ([txtQuitDate.text isEqualToString:@""]) {
        message = [message stringByAppendingString:@"\nSelect cigarette quit date."];
    }
    if ([txtQuitTime.text isEqualToString:@""]) {
        message = [message stringByAppendingString:@"\nSelect cigarette quit time."];
    }
    
    if ([message isEqualToString:@""]){
        AppDelegate* sharedDelegate = [AppDelegate appDelegate];
        
        NSManagedObjectContext *context = [sharedDelegate managedObjectContext];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Setup" inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        [fetchRequest setResultType:NSDictionaryResultType];
        
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        if (fetchedObjects == nil) {
            // Handle the error.
            NSLog(@"Getting data failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
        }
        else{
            NSManagedObject *setup = [fetchedObjects objectAtIndex:0];
            [setup setValue:txtNoOfCigDaily.text forKey:@"noOfCigDaily"];
            [setup setValue:txtNoOfCigInBox.text forKey:@"noOfCigInBox"];
            [setup setValue:txtPriceOfCigBox.text forKey:@"priceOfCigBox"];
            [setup setValue:txtQuitDate.text forKey:@"quitDate"];
            [setup setValue:txtQuitTime.text forKey:@"quitTime"];

            // Save the context
            NSError *error = nil;
            if (![context save:&error]) {
                NSLog(@"Saving Failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
            }
            else{
                [self getSettings];
                NSLog(@"Setup data saved!!");
                NSLog(@"%@", fetchedObjects);
                
                [self.navigationController popViewControllerAnimated:YES];
                [Common showAlertWithTitle:@"Alert" andMessage:@"Your quit plan updated now." andController:self.navigationController];
            }
        }
    }
    else{
        [Common showAlertWithTitle:@"Alert" andMessage:message andController:self];
    }
}
@end
