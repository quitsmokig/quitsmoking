//
//  SettingsVC.h
//  Quit Smoking
//
//  Created by Quit Smoking on 01/05/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController<UITextFieldDelegate, UIActionSheetDelegate>{
    IBOutlet UITextField *txtNoOfCigDaily;
    IBOutlet UITextField *txtNoOfCigInBox;
    IBOutlet UITextField *txtPriceOfCigBox;
    IBOutlet UITextField *txtQuitDate;
    IBOutlet UITextField *txtQuitTime;
}

-(IBAction)selectDate:(id)sender;
-(IBAction)selectTime:(id)sender;
@end
