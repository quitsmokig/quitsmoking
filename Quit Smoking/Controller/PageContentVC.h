//
//  PageContentVC.h
//  Quit Smoking
//
//  Created by Quit Smoking on 24/04/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentVC : UIViewController
@property NSUInteger pageIndex;
@property UIView *customView;
@end
