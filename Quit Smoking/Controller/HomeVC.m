//
//  HomeVC.m
//  Quit Smoking
//
//  Created by Quit Smoking on 26/04/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import "HomeVC.h"
#import "AppDelegate.h"

@implementation HomeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    
    // Show navigation bar and hide default back button
    //[self.navigationController setNavigationBarHidden:NO];
    
    // Change navigation title color white
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Create array for motivational tips
    // http://zenhabits.net/10-tips-for-quitting-smoking/
    arrMotivationalTips = @[@"Commit Thyself Fully. In the quits that failed, I was only half into it. I told myself I wanted to quit, but I always felt in the back of my mind that I’d fail. I didn’t write anything down, I didn’t tell everybody (maybe my wife, but just her). This time, I wrote it down. I wrote down a plan. I blogged about it. I made a vow to my daughter. I told family and friends I was quitting. I went online and joined a quit forum. I had rewards. Many of these will be in the following tips, but the point is that I fully committed, and there was no turning back. I didn’t make it easy for myself to fail.", @"Make a Plan. You can’t just up and say, “I’m gonna quit today.” You have to prepare yourself. Plan it out. Have a system of rewards, a support system, a person to call if you’re in trouble. Write down what you’ll do when you get an urge. Print it out. Post it up on your wall, at home and at work. If you wait until you get the urge to figure out what you’re going to do, you’ve already lost. You have to be ready when those urges come.", @"Know Your Motivation. When the urge comes, your mind will rationalize. “What’s the harm?” And you’ll forget why you’re doing this. Know why you’re doing this BEFORE that urge comes. Is it for your kids? For your wife? For you health? So you can run? Because the girl you like doesn’t like smokers? Have a very good reason or reasons for quitting. List them out. Print them out. Put it on a wall. And remind yourself of those reasons every day, every urge.", @"Not One Puff, Ever (N.O.P.E.). The mind is a tricky thing. It will tell you that one cigarette won’t hurt. And it’s hard to argue with that logic, especially when you’re in the middle of an urge. And those urges are super hard to argue with. Don’t give in. Tell yourself, before the urges come, that you will not smoke a single puff, ever again. Because the truth is, that one puff WILL hurt. One puff leads to a second, and a third, and soon you’re not quitting, you’re smoking. Don’t fool yourself. A single puff will almost always lead to a recession. DO NOT TAKE A SINGLE PUFF!", @"Join a Forum. One of the things that helped the most in this quit was an online forum for quitters (quitsmoking.about.com) … you don’t feel so alone when you’re miserable. Misery loves company, after all. Go online, introduce yourself, get to know the others who are going through the exact same thing, post about your crappy experience, and read about others who are even worse than you. Best rule: Post Before You Smoke. If you set this rule and stick to it, you will make it through your urge. Others will talk you through it. And they’ll celebrate with you when you make it through your first day, day 2, 3, and 4, week 1 and beyond. It’s great fun.", @"Reward Yourself. Set up a plan for your rewards. Definitely reward yourself after the first day, and the second, and the third. You can do the fourth if you want, but definitely after Week 1 and Week2. And month 1, and month 2. And 6 months and a year. Make them good rewards, that you’ll look forward to: CDs, books, DVDs, T-shirts, shoes, a massage, a bike, a dinner out at your favorite restaurant, a hotel stay … whatever you can afford. Even better: take whatever you would have spent on smoking each day, and put it in a jar. This is your Rewards Jar. Go crazy! Celebrate your every success! You deserve it.", @"Delay. If you have an urge, wait. Do the following things: take 10 deep breaths. Drink water. Eat a snack (at first it was candy and gum, then I switched to healthier stuff like carrots and frozen grapes and pretzels). Call your support person. Post on your smoking cessation forum. Exercise. DO WHATEVER IT TAKES, BUT DELAY, DELAY, DELAY. You will make it through it, and the urge will go away. When it does, celebrate! Take it one urge at a time, and you can do it.", @"Replace Negative Habits with Positive Ones. What do you do when you’re stressed? If you currently react to stress with a cigarette, you’ll need to find something else to do. Deep breathing, self massage of my neck and shoulders, and exercise have worked wonders for me. Other habits, such as what you do first thing in the morning, or what you do in the car, or wherever you usually smoke, should be replaced with better, more positive ones. Running has been my best positive habit, altho I have a few others that replaced smoking.", @"Make it Through Hell Week, then Heck Week, and You’re Golden. The hardest part of quitting is the first two days. If you can get past that, you’ve passed the nicotine withdrawal stage, and the rest is mostly mental. But all of the first week is hell. Which is why it’s called Hell Week. After that, it begins to get easier. Second week is Heck Week, and is still difficult, but not nearly as hellish as the first. After that, it was smooth sailing for me. I just had to deal with an occasional strong urge, but the rest of the urges were light, and I felt confident I could make it through anything.", @"If You Fall, Get Up. And Learn From Your Mistakes. Yes, we all fail. That does not mean we are failures, or that we can never succeed. If you fall, it’s not the end of the world. Get up, brush yourself off, and try again. I failed numerous times before succeeding. But you know what? Each of those failures taught me something. Well, sometimes I repeated the same mistakes several times, but eventually I learned. Figure out what your obstacles to success are, and plan to overcome them in your next quit. And don’t wait a few months until your next quit. Give yourself a few days to plan and prepare, commit fully to it, and go for it!", @"THINK POSITIVE. This is the most important tip of all. I saved it for last. If you have a positive, can-do attitude, as corny as it may sound, you will succeed. Trust me. It works. Tell yourself that you can do it, and you will. Tell yourself that you can’t do it, and you definitely won’t. When things get rough, think positive! You CAN make it through the urge. You CAN make it through Hell Week. And you can."];
    // Motivational tips link http://zenhabits.net/10-tips-for-quitting-smoking/
    arrMotivationalTips = @[@"Commit Thyself Fully. In the quits that failed, I was only half into it. I told myself I wanted to quit, but I always felt in the back of my mind that I’d fail. I didn’t write anything down, I didn’t tell everybody (maybe my wife, but just her). This time, I wrote it down. I wrote down a plan. I blogged about it. I made a vow to my daughter. I told family and friends I was quitting. I went online and joined a quit forum. I had rewards. Many of these will be in the following tips, but the point is that I fully committed, and there was no turning back. I didn’t make it easy for myself to fail.", @"Make a Plan. You can’t just up and say, “I’m gonna quit today.” You have to prepare yourself. Plan it out. Have a system of rewards, a support system, a person to call if you’re in trouble. Write down what you’ll do when you get an urge. Print it out. Post it up on your wall, at home and at work. If you wait until you get the urge to figure out what you’re going to do, you’ve already lost. You have to be ready when those urges come.", @"Know Your Motivation. When the urge comes, your mind will rationalize. “What’s the harm?” And you’ll forget why you’re doing this. Know why you’re doing this BEFORE that urge comes. Is it for your kids? For your wife? For you health? So you can run? Because the girl you like doesn’t like smokers? Have a very good reason or reasons for quitting. List them out. Print them out. Put it on a wall. And remind yourself of those reasons every day, every urge.", @"Not One Puff, Ever (N.O.P.E.). The mind is a tricky thing. It will tell you that one cigarette won’t hurt. And it’s hard to argue with that logic, especially when you’re in the middle of an urge. And those urges are super hard to argue with. Don’t give in. Tell yourself, before the urges come, that you will not smoke a single puff, ever again. Because the truth is, that one puff WILL hurt. One puff leads to a second, and a third, and soon you’re not quitting, you’re smoking. Don’t fool yourself. A single puff will almost always lead to a recession. DO NOT TAKE A SINGLE PUFF!", @"Join a Forum. One of the things that helped the most in this quit was an online forum for quitters (quitsmoking.about.com) … you don’t feel so alone when you’re miserable. Misery loves company, after all. Go online, introduce yourself, get to know the others who are going through the exact same thing, post about your crappy experience, and read about others who are even worse than you. Best rule: Post Before You Smoke. If you set this rule and stick to it, you will make it through your urge. Others will talk you through it. And they’ll celebrate with you when you make it through your first day, day 2, 3, and 4, week 1 and beyond. It’s great fun.", @"Reward Yourself. Set up a plan for your rewards. Definitely reward yourself after the first day, and the second, and the third. You can do the fourth if you want, but definitely after Week 1 and Week2. And month 1, and month 2. And 6 months and a year. Make them good rewards, that you’ll look forward to: CDs, books, DVDs, T-shirts, shoes, a massage, a bike, a dinner out at your favorite restaurant, a hotel stay … whatever you can afford. Even better: take whatever you would have spent on smoking each day, and put it in a jar. This is your Rewards Jar. Go crazy! Celebrate your every success! You deserve it.", @"Delay. If you have an urge, wait. Do the following things: take 10 deep breaths. Drink water. Eat a snack (at first it was candy and gum, then I switched to healthier stuff like carrots and frozen grapes and pretzels). Call your support person. Post on your smoking cessation forum. Exercise. DO WHATEVER IT TAKES, BUT DELAY, DELAY, DELAY. You will make it through it, and the urge will go away. When it does, celebrate! Take it one urge at a time, and you can do it.", @"Replace Negative Habits with Positive Ones. What do you do when you’re stressed? If you currently react to stress with a cigarette, you’ll need to find something else to do. Deep breathing, self massage of my neck and shoulders, and exercise have worked wonders for me. Other habits, such as what you do first thing in the morning, or what you do in the car, or wherever you usually smoke, should be replaced with better, more positive ones. Running has been my best positive habit, altho I have a few others that replaced smoking.", @"Make it Through Hell Week, then Heck Week, and You’re Golden. The hardest part of quitting is the first two days. If you can get past that, you’ve passed the nicotine withdrawal stage, and the rest is mostly mental. But all of the first week is hell. Which is why it’s called Hell Week. After that, it begins to get easier. Second week is Heck Week, and is still difficult, but not nearly as hellish as the first. After that, it was smooth sailing for me. I just had to deal with an occasional strong urge, but the rest of the urges were light, and I felt confident I could make it through anything.", @"If You Fall, Get Up. And Learn From Your Mistakes. Yes, we all fail. That does not mean we are failures, or that we can never succeed. If you fall, it’s not the end of the world. Get up, brush yourself off, and try again. I failed numerous times before succeeding. But you know what? Each of those failures taught me something. Well, sometimes I repeated the same mistakes several times, but eventually I learned. Figure out what your obstacles to success are, and plan to overcome them in your next quit. And don’t wait a few months until your next quit. Give yourself a few days to plan and prepare, commit fully to it, and go for it!", @"THINK POSITIVE. This is the most important tip of all. I saved it for last. If you have a positive, can-do attitude, as corny as it may sound, you will succeed. Trust me. It works. Tell yourself that you can do it, and you will. Tell yourself that you can’t do it, and you definitely won’t. When things get rough, think positive! You CAN make it through the urge. You CAN make it through Hell Week. And you can."];
    
    // Timer to repeat motivational tips every 5 seconds
    indexOfTips = 0;
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(showMotivationalTips) userInfo:nil repeats:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    AppDelegate* sharedDelegate = [AppDelegate appDelegate];
    
    NSManagedObjectContext *context = [sharedDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Setup" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    [fetchRequest setResultType:NSDictionaryResultType];
    //[fetchRequest setReturnsDistinctResults:YES];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        // Handle the error.
        NSLog(@"Getting data failed!, Error and its Desc %@ %@", error, [error localizedDescription]);
    }
    else{
        NSLog(@"%@", fetchedObjects);
        [lblQuitDate setText:[[fetchedObjects objectAtIndex:0] valueForKey:@"quitDate"]];
        [lblTotalCigarette setText:[self calculateTotalCigarettes:[[fetchedObjects objectAtIndex:0] valueForKey:@"noOfCigDaily"] fromQuitDate:[[fetchedObjects objectAtIndex:0] valueForKey:@"quitDate"]]];
        [lblTotalDays setText:[self calculateTotalDays:[[fetchedObjects objectAtIndex:0] valueForKey:@"quitDate"]]];
        [lblTotalMoney setText:[self calculateTotalMoney:[[fetchedObjects objectAtIndex:0] valueForKey:@"noOfCigDaily"] fromQuitDate:[[fetchedObjects objectAtIndex:0] valueForKey:@"quitDate"] cigarettesInBox:[[fetchedObjects objectAtIndex:0] valueForKey:@"noOfCigInBox"] andBoxPrice:[[fetchedObjects objectAtIndex:0] valueForKey:@"priceOfCigBox"]]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSString *)calculateTotalCigarettes:(NSString *)noOfCigaretteDaily fromQuitDate:(NSString *)quitDate{
    NSString *total = @"0";
    NSInteger totalDaily = (NSInteger)[noOfCigaretteDaily integerValue];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSString *endDate = [format stringFromDate:[NSDate date]];
    
    int totalDays = [self numberOfDaysBetween:quitDate and:endDate];
    total = [NSString stringWithFormat:@"%d", totalDaily*totalDays];
    return total;
}

- (NSString *)calculateTotalDays:(NSString *)quitDate{
    NSString *total = @"0 days";
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSString *endDate = [format stringFromDate:[NSDate date]];
    
    int totalDays = [self numberOfDaysBetween:quitDate and:endDate];
    total = [NSString stringWithFormat:@"%d days", totalDays];
    return total;
}

- (NSString *)calculateTotalMoney:(NSString *)noOfCigaretteDaily fromQuitDate:(NSString *)quitDate cigarettesInBox:(NSString *)noOfCigarettesInBox andBoxPrice:(NSString *)priceOfCigaretteBox{
    NSString *total = @"0";
    NSInteger totalDaily = (NSInteger)[noOfCigaretteDaily integerValue];
    double priceOfaCig = (double)[priceOfCigaretteBox doubleValue]/(NSInteger)[noOfCigarettesInBox integerValue];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSString *endDate = [format stringFromDate:[NSDate date]];
    
    int totalDays = [self numberOfDaysBetween:quitDate and:endDate];
    total = [NSString stringWithFormat:@"$ %.2f", (totalDaily*totalDays) * priceOfaCig];
    return total;
}

- (int)numberOfDaysBetween:(NSString *)startDate and:(NSString *)endDate {
    //NSString *start = @"2010-09-01";
    //NSString *end = @"2010-12-01";
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSDate *start = [f dateFromString:startDate];
    NSDate *end = [f dateFromString:endDate];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:start
                                                          toDate:end
                                                         options:NSCalendarWrapComponents];
    return [components day];
}

- (void)showMotivationalTips{
    if(indexOfTips < arrMotivationalTips.count){
        [txtTips setText:arrMotivationalTips[indexOfTips]];
        indexOfTips ++;
    }
    else{
        indexOfTips = 0;
    }
}

@end
