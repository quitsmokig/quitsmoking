//
//  Common.m
//  Quit Smoking
//
//  Created by Quit Smoking on 26/04/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import "Common.h"

@implementation Common

+(void)addBackButtonOnNavigationController:(UINavigationController *)navController withTitle:(NSString *)title
{
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    [barButton setTitle:@""];
    //[barButton setTintColor:[UIColor greenColor]];
    [navController.navigationBar.topItem setBackBarButtonItem:barButton];
    [navController setNavigationBarHidden:NO animated:NO];
}

+(NSString *)getFormattedDate:(NSString *)strDate withFormat:(NSString *)strFormat
{
    NSRange range = [strDate rangeOfString:@" "];
    strDate = [strDate substringToIndex:range.location]; /// here this is the date with format yyyy-MM-dd
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
    NSDate *date = [dateFormatter dateFromString: strDate]; // here you can fetch date from string with define format
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:strFormat]; //@"MMM dd, yyyy" // here set format which you want...
    NSString *convertedString = [dateFormatter stringFromDate:date]; //here convert date in NSString
    return convertedString;
}

+(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message andController:(UIViewController *)controller
{
    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //[alert show];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [controller presentViewController:alert animated:YES completion:nil];
}

+ (NSString *)calculateTotalCigarettes:(NSString *)noOfCigaretteDaily fromQuitDate:(NSString *)quitDate{
    NSString *total = @"0";
    NSInteger totalDaily = (NSInteger)[noOfCigaretteDaily integerValue];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSString *endDate = [format stringFromDate:[NSDate date]];
    
    int totalDays = [self numberOfDaysBetween:quitDate and:endDate];
    total = [NSString stringWithFormat:@"%d", totalDaily*totalDays];
    return total;
}

+ (NSString *)calculateTotalDays:(NSString *)quitDate{
    NSString *total = @"0 days";
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSString *endDate = [format stringFromDate:[NSDate date]];
    
    int totalDays = [self numberOfDaysBetween:quitDate and:endDate];
    total = [NSString stringWithFormat:@"%d days", totalDays];
    return total;
}

+ (NSString *)calculateTotalMoney:(NSString *)noOfCigaretteDaily fromQuitDate:(NSString *)quitDate cigarettesInBox:(NSString *)noOfCigarettesInBox andBoxPrice:(NSString *)priceOfCigaretteBox{
    NSString *total = @"0";
    NSInteger totalDaily = (NSInteger)[noOfCigaretteDaily integerValue];
    double priceOfaCig = (double)[priceOfCigaretteBox doubleValue]/(NSInteger)[noOfCigarettesInBox integerValue];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSString *endDate = [format stringFromDate:[NSDate date]];
    
    int totalDays = [self numberOfDaysBetween:quitDate and:endDate];
    total = [NSString stringWithFormat:@"$ %.2f", (totalDaily*totalDays) * priceOfaCig];
    return total;
}

+ (int)numberOfDaysBetween:(NSString *)startDate and:(NSString *)endDate {
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSDate *start = [f dateFromString:startDate];
    NSDate *end = [f dateFromString:endDate];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:start
                                                          toDate:end
                                                         options:NSCalendarWrapComponents];
    return [components day];
}
@end
