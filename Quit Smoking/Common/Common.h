//
//  Common.h
//  Quit Smoking
//
//  Created by Quit Smoking on 26/04/16.
//  Copyright © 2016 QuitSmokingTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Common : UIViewController

+(void)addBackButtonOnNavigationController:(UINavigationController *)navController withTitle:(NSString *)title;
+(NSString *)getFormattedDate:(NSString *)strDate withFormat:(NSString *)strFormat;
+(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message andController:(UIViewController *)controller;

+ (NSString *)calculateTotalCigarettes:(NSString *)noOfCigaretteDaily fromQuitDate:(NSString *)quitDate;
+ (NSString *)calculateTotalDays:(NSString *)quitDate;
+ (NSString *)calculateTotalMoney:(NSString *)noOfCigaretteDaily fromQuitDate:(NSString *)quitDate cigarettesInBox:(NSString *)noOfCigarettesInBox andBoxPrice:(NSString *)priceOfCigaretteBox;
+ (int)numberOfDaysBetween:(NSString *)startDate and:(NSString *)endDate;
@end
